# dotHome
_Dotfile backup & restoration tool_

_Usage_

|Command|Description|
|-------|-----------|
|save|Archive current user's dotfiles|
|load [TARGET]|Restore dotfiles from target archive |

